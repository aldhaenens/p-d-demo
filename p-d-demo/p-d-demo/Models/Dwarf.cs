﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace p_d_demo.Models
{
    public class Dwarf : IPerson
    {
        public int Age { get; set; }
        public string DwarfishName { get; set; }
    }
}
