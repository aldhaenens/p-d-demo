﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace p_d_demo.Models
{
    public class Elf : IPerson
    {
        public int Age { get; set; }
        public string ElfishName { get; set; }

        public void Deconstruct(out int age, out string elfishName)
        {
            age = Age;
            elfishName = ElfishName;
        }
    }
}
