﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using p_d_demo.Models;

namespace p_d_demo
{
    class Program
    {
        static void Main(string[] args)
        {
            IPerson elf = new Elf {Age = 110, ElfishName = "Galadriel"};
            IPerson dwarf = new Dwarf {Age = 50, DwarfishName = "Durin"};

            PrintPerson(elf);
            PrintPerson(dwarf);

            Console.WriteLine();
            Console.WriteLine("With patternmatching");
            Console.WriteLine();
            /**
             * PATTERN MATCHING
             */
            PrintPersonWithPatternMatching(elf);
            PrintPersonWithPatternMatching(dwarf);

            /**
             * Deconstructing objects
             */
            Console.WriteLine();
            Console.WriteLine("Deconstructing objects");
            Console.WriteLine();

            DeconstructingElf((Elf)elf);
            Console.ReadKey();
        }

        static void PrintPerson(IPerson person)
        {
            Console.WriteLine("*** PERSON ***");
            Console.WriteLine($"Age: {person.Age}");
            if (person is Elf)
            {
                Elf elf = (Elf) person;
                Console.WriteLine($"Name in elfish: {elf.ElfishName}");
            }

            if (person is Dwarf)
            {
                Dwarf dwarf = (Dwarf) person;
                Console.WriteLine($"Name in dwarfs: {dwarf.DwarfishName}");
            }
        }

        static void PrintPersonWithPatternMatching(IPerson person)
        {
            Console.WriteLine("*** PERSON ***");
            Console.WriteLine($"Age: {person.Age}");
            if (person is Elf elf)
            {
                Console.WriteLine($"Name in elfish: {elf.ElfishName}");
            }

            if (person is Dwarf dwarf)
            {
                Console.WriteLine($"Name in dwarfs: {dwarf.DwarfishName}");
            }
        }

        static (int succeeded, int failed, int saved, int updated) UploadPersons(IEnumerable<IPerson> persons)
        {

            //E.G. loop trough ienumerable and upload them
            return (12, 2, 7, 5);
        }

        static void Tupples()
        {
            var unnamed = ("one", "two");

            var one = unnamed.Item1;
            var two = unnamed.Item2;

            (var fieldOne, var fieldTwo) = unnamed;
            var (newFieldOne, newFieldTwo) = unnamed;

            var named = (first: "one", second: "two");

            var first = named.first;
            var second = named.second;

            (first, second) = named;

            var special = ("one", "ring", "to", "rule", "them", "all");
            var (item1, _, item3, item4,item5, item6) = special;
        }

        static void DeconstructingElf(Elf elf)
        {
            (int age, string elfishName) = elf;

            Console.WriteLine($"Age: {age}");
            Console.WriteLine($"Name in elfish: {elfishName}");
        }
    }
}
